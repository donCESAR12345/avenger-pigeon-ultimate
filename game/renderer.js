// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
console.log(canvas);
ctx.font = '30px Arial';
ctx.fillText("Icons", canvas.width/2, canvas.height/2);
